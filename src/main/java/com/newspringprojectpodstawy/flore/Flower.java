package com.newspringprojectpodstawy.flore;

import org.springframework.stereotype.Component;

//Adnotacja komonent sprawia, że spring stworzy bin'a tej klasy
@Component
public class Flower {

    public String getName(){
        return "Flower";
    }

}
