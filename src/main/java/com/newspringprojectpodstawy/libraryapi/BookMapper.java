package com.newspringprojectpodstawy.libraryapi;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookMapper {

    public BookDto map(Book book){
        return BookDto.builder()
                .id(book.getId())
                .title(book.getTitle())
                .author(book.getAuthor())
                .publicationYear(book.getPublicationYear())
                .build();
    }
    public Book mapa(BookDto book){
        return new Book(book.getTitle(), book.getAuthor(), book.getPublicationYear());

    }
    public List<BookDto> mapList(List<Book> books){
        List<BookDto> bookDtoList = books.stream()
                .map(book -> this.map(book))
                .collect(Collectors.toList());

        return bookDtoList;
    }





}
