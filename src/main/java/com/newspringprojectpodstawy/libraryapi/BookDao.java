package com.newspringprojectpodstawy.libraryapi;

import com.newspringprojectpodstawy.entity.example.Car;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookDao extends CrudRepository<Book, Integer> {

    Book findById(int id);

    List<Book> findAllByTitle(String title);

    Book save(Book book);

    void deleteById(int id);




}
