package com.newspringprojectpodstawy.libraryapi;

public class IllegalBookException extends RuntimeException {
    IllegalBookException(){
        super("Missing book information");
    }
}
