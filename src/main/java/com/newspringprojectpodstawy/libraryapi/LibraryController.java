package com.newspringprojectpodstawy.libraryapi;


import com.newspringprojectpodstawy.examples.restapi.Order;
import lombok.RequiredArgsConstructor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@Slf4j
@RestController
@RequestMapping("library/book")
@RequiredArgsConstructor
public class LibraryController {

    @Autowired
    private LibraryService libraryService;

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleException(HttpServletRequest hser, Exception ex){
        log.error(hser.getRequestURI()+ " error: "  +ex.getMessage());

    }

    @ExceptionHandler(IllegalBookException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public void handleBookException(HttpServletRequest hser, Exception ex){
        log.error(hser.getRequestURI()+ " error: "  +ex.getMessage());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("add")
    public BookDto add(@RequestBody BookDto book) {
        return this.libraryService.addBook(book);
    }

    @GetMapping("get/{id}")
    public BookDto getById(@PathVariable int id){
        return this.libraryService.getBookById(id);
    }

    @GetMapping("get")
    public List<BookDto> getByTitle(@RequestParam String title){
        return this.libraryService.getBookByTitle(title);
    }

    @DeleteMapping("{id}")
    public boolean deleteById(@PathVariable int id){
        return this.libraryService.deleteBookById(id);
    }

    @PutMapping("update")
    public BookDto updateBook(@RequestBody BookDto book) {
        return this.libraryService.updateBook(book);
    }
}
