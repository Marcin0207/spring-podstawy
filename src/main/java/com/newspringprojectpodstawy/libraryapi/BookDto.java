package com.newspringprojectpodstawy.libraryapi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//DTO- Data transfer Object
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookDto {

    private int id;
    private String title;
    private String author;
    private int publicationYear;

}
