package com.newspringprojectpodstawy.libraryapi;


import lombok.RequiredArgsConstructor;
import org.hibernate.PropertyValueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class LibraryService {

    private final BookDao bookDao;
    private final BookMapper bookMapper;

    private List<Book> bookList = new ArrayList<>();

    public BookDto updateBook(BookDto book) {

        Book bookToUpdate = this.bookDao.findById(book.getId());
        String author = book.getAuthor();
        bookToUpdate.setAuthor(author);
        bookToUpdate.setPublicationYear(book.getPublicationYear());
        bookToUpdate.setTitle(book.getTitle());
        Book entity = bookDao.save(bookToUpdate);


        return bookMapper.map(entity);


  /*   Book bookToUpdate = this.bookDao.findById(book.getId());
        int bookToUpdateId = bookToUpdate.getId();
        this.bookDao.deleteById(bookToUpdateId);
        book.setId(bookToUpdateId);
        return this.bookDao.save(book);


   */

 /*       Book bookToUpdate = this.bookList.stream()
                .filter(b -> b.getId() == book.getId()) // użyć istniejącej metody bet by Id
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("book not existing"));
        bookToUpdate.setAuthor(book.getAuthor());
        bookToUpdate.setTitle(book.getTitle());
        bookToUpdate.setPublicationYear(book.getPublicationYear());
        return bookToUpdate;

*/

    }

    public BookDto getBookById(int id) {

       final Book book = this.bookDao.findById(id);
        if (book != null) {
            return this.bookMapper.map(book);
        } else throw new IllegalArgumentException("book not existing");


       /* return this.bookList.stream()
                .filter(book -> book.getId() == id)
                .findFirst()
                .orElseThrow(()->new IllegalArgumentException("book not existing"));

        */
    }

    public List<BookDto> getBookByTitle(String title) {
        List<Book> bookList = this.bookDao.findAllByTitle(title);

        return bookMapper.mapList(bookList);

    }

    public boolean deleteBookById(int id) {

        try {
            bookDao.deleteById(id);
        }catch (Exception e){
            return false;
        }
        return true;
       // return bookList.removeIf(book -> book.getId() == id);
    }

    public BookDto addBook(BookDto book) {

        if (book.getTitle() == (null) || book.getAuthor() == (null) || book.getPublicationYear() == 0) {
            throw new IllegalBookException();
        } else {
            return bookMapper.map(bookDao.save(bookMapper.mapa(book)));


        }
       /* try {
            bookDao.save(book);
        }catch (RuntimeException e){
            throw new IllegalBookException();
        } */

  //      int maxId = getHighestId();
  //      book.setId(maxId + 1);
  //      bookList.add(book);

  /*      System.out.println("Aktualny stan biblioteki: ");
        for (Book mybook : bookList) {
            System.out.println(mybook);
        } */

    }

  /*  private int getHighestId() {
        int maxId = 0;

        for (Book book : bookList) {
            int id = book.getId();
            if ((id) > maxId) {
                maxId = id;
            }
        }
        return maxId;
    } */

}
