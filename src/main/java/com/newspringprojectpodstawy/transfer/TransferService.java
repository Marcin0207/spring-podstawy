package com.newspringprojectpodstawy.transfer;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayDeque;
import java.util.Queue;
@Slf4j
@Service
public class TransferService {

    private Queue<Transfer> queue = new ArrayDeque<>();

    public void add(Transfer transfer) {
        this.queue.offer(transfer);
    }

    public void doTransfer() {

        final Transfer transfer = this.queue.poll();
        if (transfer != null) {
            log.info("Transfer started");

            log.info("transfer ended");
        }else log.info("Nothing to transfer");
    }

}
