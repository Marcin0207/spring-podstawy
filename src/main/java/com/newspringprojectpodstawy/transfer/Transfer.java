package com.newspringprojectpodstawy.transfer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Transfer {

    private String senderAccountNumber;
    private String receiverAccountNumber;
    private BigDecimal amount;

}
