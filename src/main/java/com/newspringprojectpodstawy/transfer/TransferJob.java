package com.newspringprojectpodstawy.transfer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class TransferJob {

    private TransferService transferService;

    @Scheduled(fixedDelay = 30_000, initialDelay = 10_000)
    public void doTransfer(){
        this.transferService.doTransfer();
    }

}
