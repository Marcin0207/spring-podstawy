package com.newspringprojectpodstawy.songs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class SongConfiguration {
    @Bean
    @Scope
    public Song revolutionRadio(){
        return new Song("Green Day","Revolution Radio");
    }

    @Bean
    @Scope("singleton")
    public Song jessicaKill(){
        return new Song("Sum41","Jessica Kill");
    }

    @Bean
    @Scope("prototype")
    public Song theKidsArentAlright(){
        return new Song("Offspring","The kids aren't alright");
    }
}
