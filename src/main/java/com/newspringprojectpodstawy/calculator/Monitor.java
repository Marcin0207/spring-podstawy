package com.newspringprojectpodstawy.calculator;

import org.springframework.stereotype.Component;

@Component
public class Monitor {

    public void display(Double result){
        System.out.println("The result is: "+result);
    }

}
