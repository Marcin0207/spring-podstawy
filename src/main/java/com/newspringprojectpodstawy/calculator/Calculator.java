package com.newspringprojectpodstawy.calculator;

import org.springframework.stereotype.Component;

@Component
public class Calculator {

    Monitor monitor;

    public Calculator(final Monitor monitor) {
        this.monitor = monitor;
    }

    public Double add(Double valA, Double valB) {
        double result = valA + valB;
        monitor.display(result);
        return result;
    }

    public Double subtract(Double valA, Double valB) {
        double result = valA - valB;
        monitor.display(result);
        return result;
    }

    public Double multiply(Double valA, Double valB) {
        double result = valA * valB;
        monitor.display(result);
        return result;
    }

    public Double divide(Double valA, Double valB) {
        double result = valA/valB;
        monitor.display(result);
        return result;
    }
}
