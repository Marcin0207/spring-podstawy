package com.newspringprojectpodstawy.film;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilmConfiguration {
    @Bean
    public Film matrix(){
        return new Film("Matrix",120);
    }
    @Bean
    public Film troja(){
        return new Film("Troja", 196);
    }
    @Bean
    public Film returnOfTheKing(){
        return new Film("Lotr: Return of the king", 255);
    }


}
