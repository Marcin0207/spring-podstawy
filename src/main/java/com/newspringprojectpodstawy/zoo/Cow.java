package com.newspringprojectpodstawy.zoo;

import org.springframework.stereotype.Component;

@Component
public class Cow implements Animal{
    @Override
    public String voice() {
        return "MU";
    }
}
