package com.newspringprojectpodstawy.zoo;

import org.springframework.stereotype.Component;

@Component
public class Dog implements Animal{
    @Override
    public String voice() {
        return "Woof Woof";
    }
}
