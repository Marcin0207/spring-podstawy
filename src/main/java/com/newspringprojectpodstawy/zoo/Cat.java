package com.newspringprojectpodstawy.zoo;

import org.springframework.stereotype.Component;

@Component
public class Cat implements Animal {
    @Override
    public String voice(){
        return "Miau";
    }


}
