package com.newspringprojectpodstawy.zoo;

public interface Animal {

     String voice();

}
