package com.newspringprojectpodstawy.entity.example;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CarDao extends CrudRepository<Car, Integer> {

     List<Car> findByBrand(String Brand);
     List<Car> findByModelAndColour(String model, String Colour);
     List<Car> deleteByBrandAndModel(String Brand, String Model);

     void deleteAllByBrandAndModel(String brand, String model);

     List<Car> findAllByProductionDateBefore(LocalDate date);

     boolean existsByBrandAndModel(String brand, String model);



}
