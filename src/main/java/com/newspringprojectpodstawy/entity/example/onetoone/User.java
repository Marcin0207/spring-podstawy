package com.newspringprojectpodstawy.entity.example.onetoone;


import javax.persistence.*;

@Entity
@Table
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String login;

    //relacja
    @OneToOne()
    @JoinColumn(name ="address_fk", referencedColumnName = "id" , unique = true)
    private Address address;



}
