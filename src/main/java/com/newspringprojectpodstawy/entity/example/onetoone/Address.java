package com.newspringprojectpodstawy.entity.example.onetoone;

import javax.persistence.*;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(nullable = false)
    private String city;

    @Column
    private String street;

    @Column
    private Integer buildingNumber;

    @Column
    private Integer flatNumber;
    @OneToOne(mappedBy = "address")
    private User user;

}
