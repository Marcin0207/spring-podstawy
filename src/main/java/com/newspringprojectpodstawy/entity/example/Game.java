package com.newspringprojectpodstawy.entity.example;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String title;

    @Column
    private String category;

    @Column
    private long estimatedHours;

    @Column
    private int minAge;

    @Column
    private LocalDate releaseDate;

    public Game(String title, String category, long estimatedHours, int minAge, LocalDate releaseDate) {
        this.title = title;
        this.category = category;
        this.estimatedHours = estimatedHours;
        this.minAge = minAge;
        this.releaseDate = releaseDate;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public long getEstimatedHours() {
        return estimatedHours;
    }

    public int getMinAge() {
        return minAge;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }
}
