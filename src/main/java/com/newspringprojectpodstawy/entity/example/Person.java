package com.newspringprojectpodstawy.entity.example;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    @Column(name = "last_name", length = 50, nullable = false)
    private String surname;
    private Integer age;
    private LocalDateTime creationDate;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", creationDate=" + creationDate +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public Integer getAge() {
        return age;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public Person(String firstName, String surname, Integer age, LocalDateTime creationDate) {
        this.firstName = firstName;
        this.surname = surname;
        this.age = age;
        this.creationDate = creationDate;

    }
}
