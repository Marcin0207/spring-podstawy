package com.newspringprojectpodstawy.entity.example.onetomany;

import org.springframework.data.repository.CrudRepository;

public interface TeacherDao extends CrudRepository<Teacher, Integer> {
}
