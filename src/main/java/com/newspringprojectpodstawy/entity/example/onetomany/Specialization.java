package com.newspringprojectpodstawy.entity.example.onetomany;

public enum Specialization {

    MATH,
    ENGLISH,
    BIOLOGY,
    COMPUTER_SCIENCE
}
