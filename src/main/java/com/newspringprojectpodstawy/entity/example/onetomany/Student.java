package com.newspringprojectpodstawy.entity.example.onetomany;

import javax.persistence.*;

@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String firstName;

    @Column
    private String LastName;

    @ManyToOne
    @JoinColumn(name = "teacher_fk")
    private Teacher teacher;
}
