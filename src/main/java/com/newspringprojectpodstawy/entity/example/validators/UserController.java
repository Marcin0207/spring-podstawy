package com.newspringprojectpodstawy.entity.example.validators;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("user")
public class UserController {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("create")
    public void createUser(@Valid @RequestBody UserDto userDto) {
        System.out.println(userDto);
    }

}