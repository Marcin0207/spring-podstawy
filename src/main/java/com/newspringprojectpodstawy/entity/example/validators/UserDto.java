package com.newspringprojectpodstawy.entity.example.validators;


import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ToString

public class UserDto {

    @NotNull
    @Length(min = 6,message = "login to short")
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    @Min(value = 18, message = "User is Underaged")
    private Integer age;



}
