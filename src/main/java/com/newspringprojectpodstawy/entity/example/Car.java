package com.newspringprojectpodstawy.entity.example;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "car_brand", nullable = false, length = 20)
    private String brand;

    @Column(nullable = true, length = 50)
    private String model;

    @Column()
    private LocalDate productionDate;

    @Column()
    private String colour;

    @Column()
    private LocalDateTime creationDate;

    public Car(String brand, String model, LocalDate productionDate, String colour, LocalDateTime creationDate) {
        this.brand = brand;
        this.model = model;
        this.productionDate = productionDate;
        this.colour = colour;
        this.creationDate = creationDate;
    }

    public Integer getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public String getColour() {
        return colour;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }
}
