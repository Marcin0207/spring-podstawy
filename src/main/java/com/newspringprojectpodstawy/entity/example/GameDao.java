package com.newspringprojectpodstawy.entity.example;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameDao extends CrudRepository<Game, Integer> {


    @Query("FROM Game g WHERE g.category = :category AND g.estimatedHours > :playtime")
    List<Game> findByCategoryWithTimeLongerThan(String category, long playtime);

    @Query("FROM Game g WHERE g.minAge < :age")
    List<Game> findByAge(int age);


    @Query(value = "SELECT * FROM Game g WHERE g.min_age < ?1",nativeQuery = true)
    List<Game> findByAgeNative(int age);

}
