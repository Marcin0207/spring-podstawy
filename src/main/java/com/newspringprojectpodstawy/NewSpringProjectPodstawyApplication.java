package com.newspringprojectpodstawy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewSpringProjectPodstawyApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewSpringProjectPodstawyApplication.class, args);
    }

}
