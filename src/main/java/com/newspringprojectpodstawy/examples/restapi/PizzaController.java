package com.newspringprojectpodstawy.examples.restapi;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("pizza")
public class PizzaController {

   // @RequestMapping(value = "get", method = RequestMethod.GET)
   @GetMapping("get")
    public Pizza pizza(@RequestParam double price) {
        return new Pizza(price, "familijna", Arrays.asList("cebula","czosneg","żelapenio"));
    }
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("create")
    public void cratePizza(@RequestBody Pizza pizza){
        System.out.println(pizza);
    }
}



