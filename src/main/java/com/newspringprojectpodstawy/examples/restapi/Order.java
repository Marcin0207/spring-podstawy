package com.newspringprojectpodstawy.examples.restapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@AllArgsConstructor
@Data
@ToString
public class Order {

    private int id;
    private String address;
    private double price;

}
