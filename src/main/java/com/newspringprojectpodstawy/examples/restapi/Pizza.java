package com.newspringprojectpodstawy.examples.restapi;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
public class Pizza {

    private double price;
    private String size;
    private List<String> toppings;

}
