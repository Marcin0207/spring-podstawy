package com.newspringprojectpodstawy;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootTest
class NewSpringProjectPodstawyApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void printBrans() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);

        System.out.println("[Beans]");

        for (String beanDefinition : context.getBeanDefinitionNames()) {
            System.out.println(beanDefinition);
        }

        System.out.println("[END OF BEANS]");

    }

}
