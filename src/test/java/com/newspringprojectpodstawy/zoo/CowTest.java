package com.newspringprojectpodstawy.zoo;

import com.newspringprojectpodstawy.NewSpringProjectPodstawyApplication;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CowTest {

    @Test
    void CowBeanTest(){

        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);

        //when
        Cow cow = context.getBean(Cow.class);

        //then
        assertThat(cow.voice()).isEqualTo("MU");

    }
}