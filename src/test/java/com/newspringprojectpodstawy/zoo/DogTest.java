package com.newspringprojectpodstawy.zoo;

import com.newspringprojectpodstawy.NewSpringProjectPodstawyApplication;
import com.newspringprojectpodstawy.flore.Flower;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DogTest {

    @Test
    void DogBeanTest(){
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);


        //when
        Dog dog = context.getBean(Dog.class);

        //then
        assertThat(dog.voice()).isEqualTo("Woof Woof");




    }


}