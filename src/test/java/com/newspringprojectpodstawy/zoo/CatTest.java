package com.newspringprojectpodstawy.zoo;

import com.newspringprojectpodstawy.NewSpringProjectPodstawyApplication;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CatTest {


    @Test
    void CatBeanTest(){

        //given
        AnnotationConfigApplicationContext  context = new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);

        //when
        Cat cat = context.getBean(Cat.class);

        //then
        assertThat(cat.voice()).isEqualTo("Miau");

    }
}