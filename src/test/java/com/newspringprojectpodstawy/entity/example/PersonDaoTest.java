package com.newspringprojectpodstawy.entity.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
class PersonDaoTest {
    @Autowired
    PersonDao personDao;

    private List<Person> list;

    @BeforeEach
    void setUp() {
        list = Arrays.asList(
                new Person("Michal", "Chmielewski", 13, LocalDateTime.now()),
                new Person("Jakub", "Kowalski", 25, LocalDateTime.now()),
                new Person("Adam", "Kowalski", 20, LocalDateTime.now()),
                new Person("Ewa", "Kowalska", 15, LocalDateTime.now())
        );
    }

    @Test
    void shouldAddPerson() {
        // Tworzymy obiekt klasy encyjnej bez id
        Person person = new Person("Michal", "Chmielewski",
                13, LocalDateTime.now());
        // Zapisujemy rekord do bazy danych (id jest nadawane przez sekwencje)
        this.personDao.save(person);

        //Uzywamy metody findById aby pobrac z bazy Person o danym id.
        //findById zwraca Optional<Person> stad .orElse(null).
        Person result = this.personDao.findById(person.getId())
                .orElse(null);

        //Sprawdzenie, czy udalo sie pobrac rekord z bazy
        assertThat(result).isNotNull();

        //Usuwamy rekord z bazy danych
        this.personDao.delete(result);
    }

    @Test
    void shouldRetrievePersonByLastname() {
        //given
        this.personDao.saveAll(list);

        //when
        List<Person> result = this.personDao.findBySurname("Kowalski");

        //then
        assertThat(result).hasSize(2);
    }

    @Test
    void shouldFindAllMichals() {
        //given
        this.personDao.saveAll(list);

        //when
        List<Person> result = this.personDao.findAllMichals();

        //then
        assertThat(result).hasSize(1);
    }

    @Test
    void shouldFindAllMichalsNative() {
        //given
        this.personDao.saveAll(list);

        //when
        List<Person> result = this.personDao.findAllMichalsNative();

        //then
        assertThat(result).hasSize(1);
    }

    @Test
    void shouldFindOlderThan() {
        //given
        this.personDao.saveAll(list);

        //when
        List<Person> result = this.personDao.findOlderThan(16);

        //then
        assertThat(result).hasSize(2);
    }

    @Test
    void shouldFindByFirstNameAndLastName() {
        //given
        this.personDao.saveAll(list);

        //when
        List<Person> result = this.personDao.findByFirstNameAndLastNameNative("Ewa", "Kowalska");

        //then
        assertThat(result).hasSize(1);
    }

}