package com.newspringprojectpodstawy.entity.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Transactional
@SpringBootTest
class CarDaoTest {

    List<Car> carList;
    @Autowired
    CarDao carDao;

    @BeforeEach
    void initCars() {
        carList = Arrays.asList(
                new Car("BMW", "X5", LocalDate.of(2015, 11, 11), "red", LocalDateTime.now()),
                new Car("Opel", "Astra", LocalDate.of(2005, 11, 11), "blue", LocalDateTime.now()),
                new Car("Opel", "Astra", LocalDate.of(2009, 11, 11), "black", LocalDateTime.now()),
                new Car("KIA", "Ceed", LocalDate.of(2020, 11, 11), "grey", LocalDateTime.now()),
                new Car("BMW", "i520", LocalDate.of(2016, 11, 11), "red", LocalDateTime.now()),
                new Car("Volvo", "S60", LocalDate.of(2014, 11, 11), "blue", LocalDateTime.now())
        );

    }

    @Test
    void shouldCreateCar() {
        //given
        Car car = new Car("BMW", "X5", LocalDate.of(2020, 11, 11), "red", LocalDateTime.now());
        //when
        this.carDao.save(car);

    }

    @Test
    void shouldReturnCarByBrand() {

        //when
        this.carDao.saveAll(carList);

        List<Car> result = this.carDao.findByBrand("BMW");

//cleanUp
     //   this.carDao.deleteAll(carList);
        //then
        assertThat(result).hasSize(2);

    }

    @Test
    void deleteCarByBrandAndModel() {

        //when
        this.carDao.saveAll(carList);


        List<Car> result = this.carDao.deleteByBrandAndModel("Opel", "Astra");

        //cleanUp
        //this.carDao.deleteAll(carList);
        //then
        assertThat(result).hasSize(2);

    }


    @Test
    void shouldRetrieveCarsByProductionYearBefore() {
        //given
        this.carDao.saveAll(carList);

        //when
        List<Car> result = this.carDao.findAllByProductionDateBefore(LocalDate.of(2009, 1, 1));

        //then
        assertThat(result).hasSize(1);
    }

    @Test
    void shouldReturnTrueWhenCarExistInDatabase() {
        //given
        this.carDao.saveAll(carList);

        //when
        Integer firstCarId = carList.get(0).getId();
        boolean result = this.carDao.existsById(firstCarId);

        //then
        assertThat(result).isTrue();
    }
    @Test
    void shouldReturnTrueWhenCarExistInDatabaseByBrandAndModel() {
        //given
        this.carDao.saveAll(carList);

        //when
        boolean result = this.carDao.existsByBrandAndModel("Opel", "Astra");

        //then
        assertThat(result).isTrue();
    }

    @Test
    void shouldReturnFalseWhenCarDoesntExistInDatabaseByBrandAndModel() {
        //given
        this.carDao.saveAll(carList);

        //when
        boolean result = this.carDao.existsByBrandAndModel("Fiat", "Punto");

        //then
        assertThat(result).isFalse();
    }

}
