package com.newspringprojectpodstawy.entity.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@Transactional
class GameDaoTest {

    @Autowired
    GameDao gameDao;

    private List<Game> list;

    @BeforeEach
    void setUp() {
        list = Arrays.asList(
                new Game("Witcher 3", "RPG", 60L, 18, LocalDate.of(2015,11,11)),
                new Game("Skyrim", "RPG", 200L, 12, LocalDate.of(2011,11,11)),
                new Game("Morrowind", "RPG", 1000L, 12, LocalDate.of(2000,11,11)),
                new Game("Kangurek KAO", "adventure", 30L, 6, LocalDate.of(1998,11,11)),
                new Game("Witcher 3", "RPG", 60L, 18, LocalDate.of(2015,11,11))
        );
    }

    @Test
    void shouldRetrieveGameByCategoryAndHours() {
        //given
        this.gameDao.saveAll(list);

        //when
        List<Game> result = this.gameDao.findByCategoryWithTimeLongerThan("RPG",100L);

        //then
        assertThat(result).hasSize(2);
    }

    @Test
    void shouldRetrieveGameByAge() {
        //given
        this.gameDao.saveAll(list);

        //when
        List<Game> result = this.gameDao.findByAge(10);

        //then
        assertThat(result).hasSize(1);
    }

    @Test
    void shouldRetrieveGameByAgeNative() {
        //given
        this.gameDao.saveAll(list);

        //when
        List<Game> result = this.gameDao.findByAgeNative(10);

        //then
        assertThat(result).hasSize(1);
    }


}