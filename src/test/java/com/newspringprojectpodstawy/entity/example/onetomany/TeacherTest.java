package com.newspringprojectpodstawy.entity.example.onetomany;

import com.newspringprojectpodstawy.entity.example.Person;
import com.newspringprojectpodstawy.entity.example.PersonDao;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class TeacherTest {

    private TeacherDao teacherDao;


    private List<Teacher> teachers;

    @BeforeEach
    void setUp() {
        teachers = Arrays.asList(
                new Teacher("Michal", "Chmielewski", Specialization.COMPUTER_SCIENCE),
                new Teacher("Jakub", "Kowalski", Specialization.MATH),
                new Teacher("Adam", "Kowalski", Specialization.ENGLISH),
                new Teacher("Ewa", "Kowalska", Specialization.BIOLOGY)
        );
    }


}