package com.newspringprojectpodstawy.flore;

import com.newspringprojectpodstawy.NewSpringProjectPodstawyApplication;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.concurrent.Flow;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class FlowerTest {

    @Test
    void flowerBeanTest(){

        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);


        //when
        Flower flower = context.getBean(Flower.class);

        //then
        assertThat(flower.getName()).isEqualTo("Flower");
    }

}