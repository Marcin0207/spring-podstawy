package com.newspringprojectpodstawy.songs;

import com.newspringprojectpodstawy.NewSpringProjectPodstawyApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SongConfigurationTest {

    @Test
    void songBeanTest() {
        //given
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);
        //when
        Song song = (Song) context.getBean("revolutionRadio");

        //then
        assertThat(song.getTitle()).isEqualTo("Revolution Radio");
    }

    @Test
    void songSingletonScopeTest() {
        //given
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);
        //when
        Song song = (Song) context.getBean("jessicaKill");
        song.setAuthor("Sum42");
        song = (Song) context.getBean("jessicaKill");

        //then
        assertThat(song.getAuthor()).isEqualTo("Sum42");
    }

    @Test
    void songPrototypeScopeTest() {
        //given
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);
        //when
        Song song = (Song) context.getBean("theKidsArentAlright");
        song.setTitle("Test2");
        song = (Song) context.getBean("theKidsArentAlright");

        //then
        assertThat(song.getTitle()).isEqualTo("The kids aren't alright");
    }


}