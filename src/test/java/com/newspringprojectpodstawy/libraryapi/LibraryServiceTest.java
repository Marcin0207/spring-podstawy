package com.newspringprojectpodstawy.libraryapi;

import org.apache.maven.model.Notifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest

class LibraryServiceTest {

    @MockBean
    BookDao bookDao;


    @Autowired
    private LibraryService libraryService;

    @Test
    void shouldAddBook(){
        //given
        BookDto bookDto = new BookDto(0,"W górach szaleństwa", "H.P. Lovecraft", 1990);
        ArgumentCaptor<Book> captor = ArgumentCaptor.forClass(Book.class);
        when(bookDao.save(ArgumentMatchers.any())).thenReturn(new Book("W górach szaleństwa", "H.P. Lovecraft", 1990));
        //when
        libraryService.addBook(bookDto);
        //then
        Mockito.verify(bookDao).save(captor.capture());
        Book value = captor.getValue();
        //assertThat(value.getAuthor()).isEqualTo(bookDto.getAuthor());
        //assertThat(value.getTitle()).isEqualTo(bookDto.getTitle());
        //assertThat(value.getPublicationYear()).isEqualTo(bookDto.getPublicationYear());
        assertAll(
                ()->assertEquals(bookDto.getAuthor(),value.getAuthor()),
                ()->assertEquals("W górach szaleństwa",value.getTitle()),
                ()->assertEquals(bookDto.getPublicationYear(),value.getPublicationYear())
        );
    }
    @Test
    void shouldFindBookBeforeSave(){
        //given
        BookDto bookDto = new BookDto(0,"W górach szaleństwa", "H.P. Lovecraft", 1990);
        when(bookDao.findById(ArgumentMatchers.anyInt())).thenReturn((new Book("W górach szaleństwa", "H.P. Lovecraft", 1990)));
        when(bookDao.save(ArgumentMatchers.any())).thenReturn(new Book("W górach szaleństwa", "H.P. Lovecraft", 1990));
       //when
        libraryService.updateBook(bookDto);
        InOrder inOrder = Mockito.inOrder(bookDao);
        inOrder.verify(bookDao).findById(ArgumentMatchers.any());
        inOrder.verify(bookDao).save(ArgumentMatchers.any());
        inOrder.verifyNoMoreInteractions();

    }


}