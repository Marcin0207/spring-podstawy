package com.newspringprojectpodstawy.calculator;

import com.newspringprojectpodstawy.NewSpringProjectPodstawyApplication;
import com.newspringprojectpodstawy.flore.Flower;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void calculatorBeanTest(){

        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NewSpringProjectPodstawyApplication.class);


        //when
        Calculator calculator = context.getBean(Calculator.class);

        //then
        assertThat(calculator.add(6d,5d)).isEqualTo(11);
    }

}